import bpy
import os

bp_path = bpy.path.abspath("//")
file_path = os.path.join(bp_path, 'spongbob-model/Sitting.fbx')


# Import the 3D model
bpy.ops.import_scene.fbx(filepath=file_path)

# Optionally, you can access the imported object for further manipulation
imported_object = bpy.context.selected_objects[0]

# Do any additional operations with the imported object here

# Update the scene to reflect the changes
bpy.context.view_layer.update()



# Define the radii for the five spheres
radii = [1.0, 1.5, 2.0, 2.5, 3.0]

# Create five spheres with different sizes
for i, radius in enumerate(radii):
    # Calculate the x-coordinate to position the spheres side by side
    x_position = i * 4  # Adjust the spacing as needed
    
    # Create a new sphere with the specified radius and location
    bpy.ops.mesh.primitive_uv_sphere_add(radius=radius, location=(x_position, 0, 0))

# Update the scene to reflect the changes
bpy.context.view_layer.update()