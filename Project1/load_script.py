# This script allows us to integrate scripts external to Blender
# We do not have to reload every time we make a change in the source script using VSC

import bpy                          # Blender Python Module
import os                           # Operating System Module
import sys                          # System Module

